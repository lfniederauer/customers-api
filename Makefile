.PHONY: test all
.DEFAULT_GOAL := test

init:
	pip install -r requirements.txt

venv:
	virtualenv -p /usr/bin/python .venv

test:
	docker exec -it django pytest -s

cov:
	docker exec -it django coverage run -m pytest
	docker exec -it django coverage html
	firefox htmlcov/index.html

code-convention:
	docker exec -it django flake8

clean:
	rm -rf htmlcov
	rm -rf .pytest_cache
