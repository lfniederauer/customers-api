# Customers Oowlish API

## Description

A delightful twelve-factor app intended to humbly showcase
an implementation for a given constrained system. 

## Requirements
- Docker Compose
- Python >= 3.6.8

## Installing and Running
Although automated csv data import could be implemented,
was opted to load the API with blank slate and give to the user
the opportunity to choose the desired import file.

Generated Django management command:

`db_import <csv file path>`

The server will spawn at port *:8000*

**For 1-command setup:**

```
docker-compose up -d
```

**Import your data:**
```
docker exec -it django python3 manage.py db_import resources/customers.csv
```
## Usage

All links needed are available in the index page.

**Access: http://localhost:8000** or
```
curl -X GET "http://127.0.0.1:8000/customers/" -H  "accept: application/json"
```

#### GraphQL
**Access: http://127.0.0.1:8000/graphql**
```
query {
  allCustomers {
    id, firstName, lastName
  }
}

query {
  customerById(key: 1) {
    id, firstName, lastName, lat, lon
  }
}
```
## Quality and Tests

#### Run tests
```
docker exec -it django pytest -s
```
#### Code convention
```
docker exec -it django pycodestyle
```
```
docker exec -it django flake8
```
## Development Environment

Create a Virtual Environment
```
make venv
```

**Remember** to activate the environment
```
source .venv/bin/activate
```

Install requirements
```
make init
```

Run from Local Python Interpreter
```
python3 manage.py runserver
```
