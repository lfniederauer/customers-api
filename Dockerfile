#!/bin/bash
FROM alpine:3.8

RUN apk update && apk add bash \
        gcc \
        musl-dev \
        postgresql-dev \
        python3 \
        python3-dev \
        uwsgi-python3

RUN addgroup -S django && adduser -S -G django django
RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app

ADD requirements.txt /usr/src/app/
RUN pip3 install --upgrade pip && pip3 install -r requirements.txt

ENV DJANGO_SETTINGS_MODULE oowlish.settings

COPY uwsgi-entrypoint.ini /configs/
RUN ["chmod", "+x", "/configs/uwsgi-entrypoint.ini"]

EXPOSE 8000
