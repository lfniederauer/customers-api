import requests
import json

API_KEY = 'AIzaSyBNxaJiOkAvKoPdFyi44SS6qAz9zmg8h1M'
API_URL = 'https://maps.googleapis.com/maps/api/geocode/json'


def get_google_lat_lon(address):
    params = {
        'key': API_KEY,
        'address': address
    }
    requests.get(API_URL, params=params)
    response = requests.get(API_URL,
                            params=params)
    lat, lon = 0, 0
    if response.ok:
        data = json.loads(response.text)
        if data.get('results', False):
            location = data['results'][0]['geometry']['location']
            lat, lon = location['lat'], location['lng']
    return lat, lon
