from unittest import TestCase
from unittest import mock
import json
from services.google_geo_location import get_google_lat_lon


class TestGeoLocation(TestCase):
    """test the geolocations API request"""
    def setUp(self) -> None:
        self.response_text = {
            'results': [{
                'geometry': {'location': {
                    'lat': 43.2556568,
                    'lng': -71.8334145
                }}
            }],
            'status': 'OK'
        }

    def _mock_response(
            self,
            status=200,
            json_data=None):
        mock_resp = mock.Mock()
        return mock_resp

    @mock.patch('requests.get')
    def test_google_query(self, mock_get):
        """test google query method"""
        mock_resp = self._mock_response(status=200)
        mock_resp.ok = True
        mock_resp.text = json.dumps(self.response_text)
        mock_get.return_value = mock_resp
        lat, lon = get_google_lat_lon('Warner, NH')
        self.assertEqual(lat, 43.2556568)
        self.assertEqual(lon, -71.8334145)
