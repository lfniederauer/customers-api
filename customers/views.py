from rest_framework.generics import ListAPIView, RetrieveAPIView
from .models import Customer
from .serializers import CustomerSerializer


class CustomerAPIView(ListAPIView):
    """
    This view returns the customers list in JSON.
    """
    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()
    http_method_names = [u'get', u'head', u'options']


class CustomerRetrieveAPIView(RetrieveAPIView):
    """
    This view returns a particular customers according to the id.
    """
    serializer_class = CustomerSerializer
    lookup_field = 'id'
    queryset = Customer.objects.all()
    http_method_names = [u'get', u'head', u'options']
