import graphene
from graphene_django import DjangoObjectType

from customers.models import Customer
"""
"id","first_name","last_name","email","gender","company","city","title","lat","lon"
"""
class CustomerType(DjangoObjectType):
    class Meta:
        model = Customer
        fields = "__all__"


class Query(graphene.ObjectType):
    all_customers = graphene.List(CustomerType)
    customer_by_id = graphene.Field(CustomerType, key=graphene.Int())

    def resolve_all_customers(root, info):
        return Customer.objects.all()

    def resolve_customer_by_id(root, info, key):
        return Customer.objects.get(pk=key)


schema = graphene.Schema(query=Query)
