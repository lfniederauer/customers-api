from django.db.models import (Model,
                              CharField,
                              DecimalField,
                              EmailField,
                              TextChoices)


class Customer(Model):
    """
    Simple model provided as is.
    """
    Genders = TextChoices('Female', 'Male')
    first_name = CharField("customer's first name", max_length=62)
    last_name = CharField("customer's last name", max_length=62)
    email = EmailField("customer's email")
    gender = CharField("customer's gender", choices=Genders.choices,
                       max_length=6)
    company = CharField("customer's company name", max_length=62)
    city = CharField("customer's first name", max_length=62)
    title = CharField("customer's first name", max_length=62)
    lat = DecimalField(max_digits=22, decimal_places=16, blank=True, null=True)
    lon = DecimalField(max_digits=22, decimal_places=16, blank=True, null=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    class Meta:
        verbose_name = "Customer",
        verbose_name_plural = "Customers"
