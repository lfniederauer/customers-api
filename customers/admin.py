from django.contrib import admin
from .models import Customer


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'company', 'city', 'title')
    list_filter = ['gender']

    class Meta:
        model = Customer


admin.site.register(Customer, CustomerAdmin)
