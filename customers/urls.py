from django.urls import path
from .views import CustomerAPIView, CustomerRetrieveAPIView

app_name = "customers"

urlpatterns = [
    path('', CustomerAPIView.as_view(), name='list'),
    path('<int:id>/', CustomerRetrieveAPIView.as_view(), name='detail'),
]
