from django.core.management.base import BaseCommand
from customers.models import Customer
from services.google_geo_location import get_google_lat_lon
import csv

'''
id,first_name,last_name,email,gender,company,city,title
lat,lon
'''


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('path',
                            type=str,
                            help='Provide the path os the file to be imported')

    def handle(self, *args, **options):
        try:
            with open(options['path'], newline='') as csvfile:
                csvfile.readline()
                customers_reader = csv.reader(csvfile, delimiter=',')
                for row in customers_reader:
                    lat, lon = get_google_lat_lon(row[6])
                    customer = Customer(id=row[0],
                                        first_name=row[1],
                                        last_name=row[2],
                                        email=row[3],
                                        gender=row[4],
                                        company=row[5],
                                        city=row[6],
                                        title=row[7],
                                        lat=lat,
                                        lon=lon
                                        )
                    customer.save()
                    self.stdout.write(
                        f'{customer.first_name} saved with lat lon.',
                        ending='\n')
        except FileNotFoundError:
            self.stderr.write(f'File not found at {options["path"]}',
                              ending='\n')
