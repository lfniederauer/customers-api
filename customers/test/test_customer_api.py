from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from customers.models import Customer

from customers.serializers import CustomerSerializer

CUSTOMERS_URL = reverse('customers:list')


class CustomersApiTest(TestCase):
    """Test the Customer API"""

    def setUp(self):
        self.client = APIClient()

    def test_login_not_required(self):
        """Test that this endpoint is public, has no restraint"""
        res = self.client.get(CUSTOMERS_URL)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_retrieve_customer_list(self):
        """Test retrieving customer list"""
        Customer.objects.create(first_name='Jean Paul', last_name='Sartre')
        Customer.objects.create(first_name='Jerry', last_name='Lewis')

        res = self.client.get(CUSTOMERS_URL)
        customers = Customer.objects.all().order_by('pk')
        serializer = CustomerSerializer(customers, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_retrieve_customer(self):
        """Test retrieving specific customer"""
        Customer.objects.create(first_name='Jean Paul', last_name='Sartre')

        res = self.client.get(f'{CUSTOMERS_URL}1/')
        self.assertEqual(res.status_code, status.HTTP_200_OK)
