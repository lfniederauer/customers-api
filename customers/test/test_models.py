from customers.models import Customer
from django.test import TestCase


class TestModel(TestCase):

    def setUp(self) -> None:
        """Test creting a new customer"""
        self.data = dict(
            first_name='Barack',
            last_name='Obama',
            email='the@guy.com',
            gender='Male',
            company='POTUS',
            city='Washington, DC',
            title='President',
            lat=1.2,
            lon=3.4)
        self.customer = Customer(**self.data)

    def test_create_customer(self):
        self.assertEqual(self.customer.first_name, self.data['first_name'])
        self.assertEqual(self.customer.last_name, self.data['last_name'])
        self.assertEqual(self.customer.email, self.data['email'])
        self.assertEqual(self.customer.gender, self.data['gender'])
        self.assertEqual(self.customer.company, self.data['company'])
        self.assertEqual(self.customer.city, self.data['city'])
        self.assertEqual(self.customer.title, self.data['title'])
        self.assertEqual(self.customer.lat, self.data['lat'])
        self.assertEqual(self.customer.lon, self.data['lon'])

    def test_str_customer(self):
        self.assertEqual(
            str(self.customer),
            self.data['first_name'] + ' ' + self.data['last_name']
        )
