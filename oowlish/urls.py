from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import TemplateView
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf.urls.static import static
from django.conf import settings
from graphene_django.views import GraphQLView

swagger_info = openapi.Info(
    title="Customers API",
    default_version='v1',
    description="A Customers API Implementation",
    terms_of_service="https://www.google.com/policies/terms/",
    contact=openapi.Contact(email="lfniederauer@gmail.com"),
    license=openapi.License(name="MIT"),
)

SchemaView = get_schema_view(
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    re_path('^swagger(?P<format>.json|.yaml)$',
            SchemaView.without_ui(cache_timeout=0),
            name='schema-json'),
    path('swagger/',
         SchemaView.with_ui('swagger', cache_timeout=0),
         name='schema-swagger-ui'),
    path('redoc/',
         SchemaView.with_ui('redoc', cache_timeout=0),
         name='schema-redoc'),
    path("graphql", GraphQLView.as_view(graphiql=True), name='graphql'),
    path('admin/', admin.site.urls),
    path('customers/',
         include('customers.urls', namespace='customers')),
    path('', TemplateView.as_view(template_name='home.html'),
         name='home')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
